#TODO: work in progress...
module NewRelic
  class API < Grape::API
    version 'v1', using: :header, vendor: 'newrelic'
    format :json
    prefix :api

    helpers do
      def current_user
        @current_user ||= User.authorize!(env)
      end
      def authenticate!
        error!('401 Unauthorized', 401) unless current_user
      end

      resource :applications do
        desc 'List of the Applications associated with your New Relic account'
        get :Applications do
          Status.limit(20)
        end
      end

    end #end helpers
  end #end class
end #end modules
