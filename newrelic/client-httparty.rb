require 'HTTParty'

headers = {
  "X-Api-Key" => "28693ff5a2a16467517fab2e01f699b8"
}
response = HTTParty.get(
  "https://infra-api.newrelic.com/v2/alerts/conditions?policy_id=436521",
  :headers => headers
)
puts response.parsed_response
